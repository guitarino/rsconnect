use crate::{
	connect_dependencies::ConnectDependencies,
	connect_structs::Connect,
	node_traits::{AnyDynNode, DynComputedNode, RefNode},
	resolution::ResolutionVisitor,
};

pub(crate) trait ConnectUpdate {
	fn get_update_list(
		&self,
		modified_nodes: &mut Vec<AnyDynNode>,
	) -> Vec<DynComputedNode>;

	fn visit_node(
		&self,
		resolution_visitor: ResolutionVisitor,
		node: DynComputedNode,
	) -> ResolutionVisitor;

	fn recalculate_update_list(
		&mut self,
		modified_nodes: &mut Vec<AnyDynNode>,
		update_list: Vec<DynComputedNode>,
	);
}

impl ConnectUpdate for Connect {
	fn get_update_list(
		&self,
		modified_nodes: &mut Vec<AnyDynNode>,
	) -> Vec<DynComputedNode> {
		let mut resolution_visitor = ResolutionVisitor::new();
		for node in modified_nodes {
			for derived in node.get_derived().iter() {
				if let Some(derived) = derived.upgrade() {
					resolution_visitor = self.visit_node(resolution_visitor, derived);
				}
			}
		}
		resolution_visitor.get_resolved()
	}

	fn visit_node(
		&self,
		mut resolution_visitor: ResolutionVisitor,
		node: DynComputedNode,
	) -> ResolutionVisitor {
		if !resolution_visitor.should_enter(&node) {
			return resolution_visitor;
		}
		resolution_visitor.pre(&node);
		for derived in node.borrow().get_derived() {
			if let Some(derived) = derived.upgrade() {
				resolution_visitor = self.visit_node(resolution_visitor, derived);
			}
		}
		resolution_visitor.post(&node);
		resolution_visitor
	}

	fn recalculate_update_list(
		&mut self,
		modified_nodes: &mut Vec<AnyDynNode>,
		update_list: Vec<DynComputedNode>,
	) {
		for computed in update_list {
			if self.is_any_dependency_changed(&computed, modified_nodes) {
				self.remove_computed_from_derived_lists(&computed);
				self.recalculate_value_and_set_dependencies(&computed, modified_nodes);
				self.add_computed_to_derived_lists(&computed);
			}
		}
	}
}
