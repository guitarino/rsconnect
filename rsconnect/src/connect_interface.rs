use std::{cell::RefCell, rc::Rc};

use crate::{
	comparable_value::{ComparableValue, NonComparableValue},
	connect_dependencies::ConnectDependencies,
	connect_node_update::NodeUpdate,
	connect_structs::Connect,
	node_structs::{
		Computed, ConnectComputed, ConnectComputedAny, ConnectEffected,
		ConnectEffectedAny, ConnectObserved, ConnectObservedAny, Effected, Observed,
	},
	node_traits::{ComputedNode, DynComputedNode},
};

pub trait ConnectInterface {
	fn observed<T: PartialEq + 'static>(&mut self, value: T) -> ConnectObserved<T>;

	fn observed_any<T: 'static>(&mut self, value: T) -> ConnectObservedAny<T>;

	fn computed<T: PartialEq + 'static, F: Fn(&mut Connect) -> T + 'static>(
		&mut self,
		comp_fun: F,
	) -> ConnectComputed<T, F>;

	fn computed_any<T: 'static, F: Fn(&mut Connect) -> T + 'static>(
		&mut self,
		comp_fun: F,
	) -> ConnectComputedAny<T, F>;

	fn effected<
		T: PartialEq + 'static,
		F: Fn(&mut Connect) -> T + 'static,
		E: FnMut(&T) + 'static,
	>(
		&mut self,
		comp_fun: F,
		effect: E,
	) -> ConnectEffected<T, F, E>;

	fn effected_any<
		T: 'static,
		F: Fn(&mut Connect) -> T + 'static,
		E: FnMut(&T) + 'static,
	>(
		&mut self,
		comp_fun: F,
		effect: E,
	) -> ConnectEffectedAny<T, F, E>;
}

impl ConnectInterface for Connect {
	fn observed<T: PartialEq + 'static>(&mut self, value: T) -> ConnectObserved<T> {
		Rc::new(RefCell::new(Observed::<T, ComparableValue<T>>::new(value)))
	}

	fn observed_any<T: 'static>(&mut self, value: T) -> ConnectObservedAny<T> {
		Rc::new(RefCell::new(Observed::<T, NonComparableValue<T>>::new(
			value,
		)))
	}

	fn computed<T: PartialEq + 'static, F: Fn(&mut Connect) -> T + 'static>(
		&mut self,
		comp_fun: F,
	) -> ConnectComputed<T, F> {
		let (value, dependencies) = self.get_computed_value(&comp_fun);
		let computed = Rc::new(RefCell::new(Computed::<T, ComparableValue<T>, F>::new(
			comp_fun,
			value,
			dependencies,
		)));
		let computed_clone = computed.clone() as DynComputedNode;
		self.add_computed_to_derived_lists(&computed_clone);
		computed
	}

	fn computed_any<T: 'static, F: Fn(&mut Connect) -> T + 'static>(
		&mut self,
		comp_fun: F,
	) -> ConnectComputedAny<T, F> {
		let (value, dependencies) = self.get_computed_value(&comp_fun);
		let computed = Rc::new(RefCell::new(
			Computed::<T, NonComparableValue<T>, F>::new(comp_fun, value, dependencies),
		));
		let computed_clone = computed.clone() as DynComputedNode;
		self.add_computed_to_derived_lists(&computed_clone);
		computed
	}

	fn effected<
		T: PartialEq + 'static,
		F: Fn(&mut Connect) -> T + 'static,
		E: FnMut(&T) + 'static,
	>(
		&mut self,
		comp_fun: F,
		effect: E,
	) -> ConnectEffected<T, F, E> {
		let (value, dependencies) = self.get_computed_value(&comp_fun);
		let effected =
			Rc::new(RefCell::new(Effected::<T, ComparableValue<T>, F, E>::new(
				comp_fun,
				value,
				effect,
				dependencies,
			)));
		effected.borrow_mut().run_effect();
		let effected_clone = effected.clone() as DynComputedNode;
		self.add_computed_to_derived_lists(&effected_clone);
		effected
	}

	fn effected_any<
		T: 'static,
		F: Fn(&mut Connect) -> T + 'static,
		E: FnMut(&T) + 'static,
	>(
		&mut self,
		comp_fun: F,
		effect: E,
	) -> ConnectEffectedAny<T, F, E> {
		let (value, dependencies) = self.get_computed_value(&comp_fun);
		let effected = Rc::new(RefCell::new(
			Effected::<T, NonComparableValue<T>, F, E>::new(
				comp_fun,
				value,
				effect,
				dependencies,
			),
		));
		effected.borrow_mut().run_effect();
		let effected_clone = effected.clone() as DynComputedNode;
		self.add_computed_to_derived_lists(&effected_clone);
		effected
	}
}
