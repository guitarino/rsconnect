use crate::{
	connect_node_interface::ConnectNodeUpdateInitiator, connect_structs::Connect,
};

pub trait ConnectBatch {
	fn batch<F: FnOnce(&mut Connect)>(&mut self, batch_fun: F);
}

impl ConnectBatch for Connect {
	fn batch<F: FnOnce(&mut Connect)>(&mut self, batch_fun: F) {
		self.modified_nodes = Some(Vec::new());
		batch_fun(self);
		let modified_ids_option = std::mem::replace(&mut self.modified_nodes, None);
		if let Some(modified_nodes) = modified_ids_option {
			self.after_set_nodes(modified_nodes);
		}
	}
}
