use crate::node_traits::{AnyDynNode, ConnectNode};

#[derive(Default)]
pub(crate) struct Recalculation {
	pub(crate) dependencies: Vec<ConnectNode>,
}

impl Recalculation {
	pub(crate) fn new() -> Self {
		Default::default()
	}
}

#[derive(Default)]
pub struct Connect {
	pub(crate) modified_nodes: Option<Vec<AnyDynNode>>,
	pub(crate) recalculation: Option<Recalculation>,
}

impl Connect {
	pub fn new() -> Self {
		Default::default()
	}
}
