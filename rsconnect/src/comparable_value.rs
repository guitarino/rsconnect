pub struct NonComparableValue<T>(pub(crate) T);

pub struct ComparableValue<T: PartialEq>(pub(crate) T);

pub trait Value<T> {
	fn get_ref(&self) -> &T;

	fn set_own(&mut self, value: T) -> T;

	fn equal(&self, old_value: &T) -> bool;
}

impl<T> Value<T> for NonComparableValue<T> {
	fn get_ref(&self) -> &T {
		&self.0
	}

	fn set_own(&mut self, value: T) -> T {
		std::mem::replace(&mut self.0, value)
	}

	fn equal(&self, _old_value: &T) -> bool {
		false
	}
}

impl<T: PartialEq> Value<T> for ComparableValue<T> {
	fn get_ref(&self) -> &T {
		&self.0
	}

	fn set_own(&mut self, value: T) -> T {
		std::mem::replace(&mut self.0, value)
	}

	fn equal(&self, old_value: &T) -> bool {
		self.0 == *old_value
	}
}
