pub(crate) fn is_value_pointer_equal<T: ?Sized>(left: *const T, right: *const T) -> bool {
	std::ptr::eq(left as *const (), right as *const ())
}
