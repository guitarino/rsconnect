use common::Count;
use rsconnect::{clone, Connect, ConnectInterface, ConnectNodeInterface};
mod common;

#[test]
fn avoid_unnecessary_updates() {
	let mut c = Connect::new();

	let y_calls: Count = Default::default();
	let y_calls_closure = y_calls.clone();
	let z_calls: Count = Default::default();
	let z_calls_closure = z_calls.clone();

	let x = c.observed(1);
	let y = c.computed(clone!(move |c| {
		y_calls_closure.increase();
		if *c.get(&x) >= 0 {
			1
		} else {
			-1
		}
	}));
	let z = c.computed(clone!(move |c| {
		z_calls_closure.increase();
		*c.get(&y) * 5
	}));

	assert_eq!(y_calls.get(), 1);
	assert_eq!(z_calls.get(), 1);

	c.set(&x, -1);

	assert_eq!(*c.get(&y), -1);
	assert_eq!(*c.get(&z), -5);
	assert_eq!(y_calls.get(), 2);
	assert_eq!(z_calls.get(), 2);

	c.set(&x, -6);
	assert_eq!(*c.get(&y), -1);
	assert_eq!(*c.get(&z), -5);
	assert_eq!(y_calls.get(), 3);
	assert_eq!(z_calls.get(), 2);

	c.set(&x, -6);
	assert_eq!(*c.get(&y), -1);
	assert_eq!(*c.get(&z), -5);
	assert_eq!(y_calls.get(), 3);
	assert_eq!(z_calls.get(), 2);
}
