use rsconnect::{
	clone, Connect, ConnectInterface, ConnectNodeInterface, NodeValueAccess,
};
use std::time::Instant;

#[test]
fn performance_basic() {
	let (result_connect, time_connect) = {
		let now = Instant::now();
		let mut conn = Connect::new();

		let a = conn.observed(1);
		let b = conn.observed(2);

		let c = conn.computed(clone!(move |conn| { *conn.get(&a) + *conn.get(&b) }));
		let d = conn.computed(clone!(move |conn| {
			*conn.get(&a) + *conn.get(&b) + *conn.get(&c)
		}));
		let e = conn.computed(clone!(move |conn| {
			*conn.get(&a) + *conn.get(&b) + *conn.get(&c) + *conn.get(&d)
		}));

		for a_val in 1..100 {
			conn.set(&a, a_val);

			for b_val in 1..100 {
				conn.set(&b, b_val);
			}
		}

		let e_result = *e.get_inert();
		(e_result, now.elapsed().as_micros())
	};

	let (result_manual, time_manual) = {
		let now = Instant::now();

		let mut a = 1;
		let mut b = 2;
		let mut c = -100;
		let mut d = -100;
		let mut e = -100;

		for a_val in 1..100 {
			let mut a_changed = false;
			if a_val != a {
				a = a_val;
				a_changed = true;
			}

			for b_val in 1..100 {
				let mut b_changed = false;

				if b_val != b {
					b = b_val;
					b_changed = true;
				}

				let mut c_changed = false;
				if a_changed || b_changed {
					let c_val = a + b;

					if c_val != c {
						c = c_val;
						c_changed = true;
					}
				}

				let mut d_changed = false;
				if a_changed || b_changed || c_changed {
					let d_val = a + b + c;

					if d_val != d {
						d = d_val;
						d_changed = true;
					}
				}

				if a_changed || b_changed || c_changed || d_changed {
					let e_val = a + b + c + d;

					if e_val != e {
						e = e_val;
					}
				}
			}
		}

		(e, now.elapsed().as_micros())
	};

	assert_eq!(result_connect, 792);
	assert_eq!(result_manual, 792);

	println!("Connect time: {}", time_connect);
	println!("Manual time: {}", time_manual);
	println!(
		"Connect is {} times slower",
		time_connect as f32 / time_manual as f32
	);
}
