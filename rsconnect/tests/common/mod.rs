use std::{cell::RefCell, rc::Rc};

#[derive(Default, Clone)]
pub struct Count {
	n: Rc<RefCell<u32>>,
}

impl Count {
	#[allow(dead_code)]
	pub fn set(&self, value: u32) {
		*self.n.borrow_mut() = value;
	}

	#[allow(dead_code)]
	pub fn increase(&self) {
		let n = self.get();
		*self.n.borrow_mut() = n + 1;
	}

	#[allow(dead_code)]
	pub fn get(&self) -> u32 {
		*self.n.borrow()
	}
}
