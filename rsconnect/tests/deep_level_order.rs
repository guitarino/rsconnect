use common::Count;
use rsconnect::{clone, Connect, ConnectInterface, ConnectNodeInterface};
mod common;

#[test]
fn deep_level_order() {
	let mut conn = Connect::new();

	let b_calls: Count = Default::default();
	let b_calls_closure = b_calls.clone();
	let c_calls: Count = Default::default();
	let c_calls_closure = c_calls.clone();
	let d_calls: Count = Default::default();
	let d_calls_closure = d_calls.clone();
	let e_calls: Count = Default::default();
	let e_calls_closure = e_calls.clone();
	let f_calls: Count = Default::default();
	let f_calls_closure = f_calls.clone();
	let g_calls: Count = Default::default();
	let g_calls_closure = g_calls.clone();

	let a = conn.observed(1);
	let b = conn.computed(clone!(move |conn| {
		b_calls_closure.increase();
		*conn.get(&a) + 10
	}));
	let c = conn.computed(clone!(move |conn| {
		c_calls_closure.increase();
		*conn.get(&a) + 200
	}));
	let d = conn.computed(clone!(move |conn| {
		d_calls_closure.increase();
		*conn.get(&b) + 3000
	}));
	let e = conn.computed(clone!(move |conn| {
		e_calls_closure.increase();
		*conn.get(&d) + 40000
	}));
	let f = conn.computed(clone!(move |conn| {
		f_calls_closure.increase();
		*conn.get(&e) + *conn.get(&c)
	}));
	let g = conn.computed(clone!(move |conn| {
		g_calls_closure.increase();
		*conn.get(&f) + 1000
	}));

	assert_eq!(b_calls.get(), 1);
	assert_eq!(c_calls.get(), 1);
	assert_eq!(d_calls.get(), 1);
	assert_eq!(e_calls.get(), 1);
	assert_eq!(f_calls.get(), 1);
	assert_eq!(g_calls.get(), 1);

	assert_eq!(*conn.get(&g), 44212);

	conn.set(&a, 2);

	assert_eq!(*conn.get(&g), 44214);

	assert_eq!(b_calls.get(), 2);
	assert_eq!(c_calls.get(), 2);
	assert_eq!(d_calls.get(), 2);
	assert_eq!(e_calls.get(), 2);
	assert_eq!(f_calls.get(), 2);
	assert_eq!(g_calls.get(), 2);
}
