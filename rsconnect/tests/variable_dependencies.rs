use common::Count;
use rsconnect::{clone, Connect, ConnectInterface, ConnectNodeInterface};
mod common;

#[test]
fn variable_dependencies() {
	let mut conn = Connect::new();

	let e_calls: Count = Default::default();
	let e_calls_closure = e_calls.clone();

	let flag = conn.observed(true);
	let a = conn.observed(1);
	let b = conn.observed(2);
	let c = conn.observed(100);
	let d = conn.observed(200);

	let e = conn.computed(clone!(move |conn| {
		e_calls_closure.increase();
		if *conn.get(&flag) {
			*conn.get(&a) + *conn.get(&b)
		} else {
			*conn.get(&c) + *conn.get(&d)
		}
	}));

	assert_eq!(*conn.get(&e), 3);

	conn.set(&a, 5);
	assert_eq!(*conn.get(&e), 7);
	assert_eq!(e_calls.get(), 2);

	conn.set(&b, 10);
	assert_eq!(*conn.get(&e), 15);
	assert_eq!(e_calls.get(), 3);

	conn.set(&c, 500);
	assert_eq!(*conn.get(&e), 15);
	assert_eq!(e_calls.get(), 3);

	conn.set(&d, 1000);
	assert_eq!(*conn.get(&e), 15);
	assert_eq!(e_calls.get(), 3);

	conn.set(&flag, false);
	assert_eq!(*conn.get(&e), 1500);
	assert_eq!(e_calls.get(), 4);

	conn.set(&a, 6);
	assert_eq!(*conn.get(&e), 1500);
	assert_eq!(e_calls.get(), 4);

	conn.set(&b, 20);
	assert_eq!(*conn.get(&e), 1500);
	assert_eq!(e_calls.get(), 4);

	conn.set(&c, 600);
	assert_eq!(*conn.get(&e), 1600);
	assert_eq!(e_calls.get(), 5);

	conn.set(&d, 1100);
	assert_eq!(*conn.get(&e), 1700);
	assert_eq!(e_calls.get(), 6);
}
