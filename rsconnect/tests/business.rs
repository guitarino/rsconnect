use rsconnect::{
	clone, Connect, ConnectBatch, ConnectInterface, ConnectNodeInterface, DynComputedNode,
};
mod common;

#[test]
fn business() {
	let mut c = Connect::new();
	let mut my_effects: Vec<DynComputedNode> = Vec::new();

	let items_produced = c.observed(25);
	let items_sold = c.observed(20);
	let item_cost = c.observed(10.0);
	let item_price = c.observed(13.0);
	let revenue = c.computed(clone!(
		move |c| *c.get(&item_price) * *c.get(&items_sold) as f32
	));
	let total_cost = c.computed(clone!(
		move |c| *c.get(&item_cost) * *c.get(&items_produced) as f32
	));
	let profit = c.computed(clone!(move |c| *c.get(&revenue) - *c.get(&total_cost)));

	my_effects.push(c.effected(clone!(move |c| *c.get(&profit)), |profit| {
		println!("Profit: {}", profit)
	}));

	assert_eq!(*c.get(&profit), 10.0);

	c.set(&items_sold, 25);

	assert_eq!(*c.get(&profit), 75.0);

	c.batch(|c| {
		c.set(&items_produced, 60);
		c.set(&items_sold, 55);
	});

	assert_eq!(*c.get(&profit), 115.0);

	assert_eq!(my_effects.len(), 1);
}
