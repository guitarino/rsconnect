use common::Count;
use rsconnect::{
	clone, Connect, ConnectInterface, ConnectNodeInterface, DynComputedNode,
	NodeValueAccess,
};
mod common;

#[test]
fn effected() {
	let mut conn = Connect::new();
	let mut effects: Vec<DynComputedNode> = Vec::new();

	let effect_1_calls: Count = Default::default();
	let effect_1_calls_closure: Count = effect_1_calls.clone();
	let effect_1_result: Count = Default::default();
	let effect_1_result_closure: Count = effect_1_result.clone();
	let effect_2_calls: Count = Default::default();
	let effect_2_calls_closure: Count = effect_2_calls.clone();
	let effect_2_result: Count = Default::default();
	let effect_2_result_closure: Count = effect_2_result.clone();

	let a = conn.observed(100_u32);
	let b = conn.observed(0_u32);

	effects.push(conn.effected(clone!(move |conn| *conn.get(&a)), {
		let b = b.clone();
		move |a| {
			effect_1_calls_closure.increase();
			effect_1_result_closure.set(a + *b.get_inert());
		}
	}));

	effects.push(conn.effected(clone!(move |conn| *conn.get(&a)), {
		let b = b.clone();
		move |a| {
			effect_2_calls_closure.increase();
			effect_2_result_closure.set(a + *b.get_inert());
		}
	}));

	assert_eq!(effect_1_calls.get(), 1);
	assert_eq!(effect_1_result.get(), 100);

	conn.set(&a, 200);

	assert_eq!(effect_1_calls.get(), 2);
	assert_eq!(effect_1_result.get(), 200);

	conn.set(&b, 300);

	assert_eq!(effect_1_calls.get(), 2);

	conn.set(&a, 400);

	assert_eq!(effect_1_calls.get(), 3);
	assert_eq!(effect_1_result.get(), 700);

	assert_eq!(effect_2_calls.get(), 3);
	assert_eq!(effect_2_result.get(), 700);

	assert_eq!(effects.len(), 2);
}
