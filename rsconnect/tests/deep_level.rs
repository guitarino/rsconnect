use common::Count;
use rsconnect::{clone, Connect, ConnectInterface, ConnectNodeInterface};
mod common;

#[test]
fn deep_level() {
	let mut conn = Connect::new();

	let c_calls: Count = Default::default();
	let d_calls: Count = Default::default();
	let e_calls: Count = Default::default();
	let c_calls_closure = c_calls.clone();
	let d_calls_closure = d_calls.clone();
	let e_calls_closure = e_calls.clone();

	let a = conn.observed(1);
	let b = conn.observed(2);
	let c = conn.computed(clone!(move |conn| {
		c_calls_closure.increase();
		*conn.get(&a) + *conn.get(&b)
	}));
	let d = conn.computed(clone!(move |conn| {
		d_calls_closure.increase();
		*conn.get(&a) + *conn.get(&b) + *conn.get(&c)
	}));
	let e = conn.computed(clone!(move |conn| {
		e_calls_closure.increase();
		*conn.get(&a) + *conn.get(&b) + *conn.get(&c) + *conn.get(&d)
	}));

	assert_eq!(*conn.get(&e), 12);
	assert_eq!(c_calls.get(), 1);
	assert_eq!(d_calls.get(), 1);
	assert_eq!(e_calls.get(), 1);

	conn.set(&a, 100);
	assert_eq!(*conn.get(&e), 408);

	assert_eq!(c_calls.get(), 2);
	assert_eq!(d_calls.get(), 2);
	assert_eq!(e_calls.get(), 2);
}
