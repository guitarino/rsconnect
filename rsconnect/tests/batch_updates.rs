use common::Count;
use rsconnect::{clone, Connect, ConnectBatch, ConnectInterface, ConnectNodeInterface};
mod common;

#[test]
fn batch_updates() {
	let mut conn = Connect::new();

	let b_calls: Count = Default::default();
	let b_calls_closure = b_calls.clone();
	let c_calls: Count = Default::default();
	let c_calls_closure = c_calls.clone();

	let a = conn.observed(18);
	let x = conn.observed(0);
	let y = conn.observed(0);
	let z = conn.observed(0);
	let b = conn.computed(clone!(move |conn| {
		b_calls_closure.increase();
		*conn.get(&a) + *conn.get(&x) + *conn.get(&y) + 3
	}));
	let c = conn.computed(clone!(move |conn| {
		c_calls_closure.increase();
		*conn.get(&a) + *conn.get(&b) + *conn.get(&z)
	}));

	assert_eq!(b_calls.get(), 1);
	assert_eq!(c_calls.get(), 1);

	conn.batch(|conn| {
		conn.set(&a, 20);
		conn.set(&x, 1);
		conn.set(&y, 2);
		conn.set(&z, 3);
		assert_eq!(b_calls.get(), 1);
		assert_eq!(c_calls.get(), 1);
	});

	assert_eq!(b_calls.get(), 2);
	assert_eq!(c_calls.get(), 2);
	assert_eq!(*conn.get(&b), 26);
	assert_eq!(*conn.get(&c), 49);

	conn.batch(|conn| {
		conn.set(&z, 4);
		assert_eq!(b_calls.get(), 2);
		assert_eq!(c_calls.get(), 2);
	});

	assert_eq!(b_calls.get(), 2);
	assert_eq!(c_calls.get(), 3);
	assert_eq!(*conn.get(&c), 50);
}
