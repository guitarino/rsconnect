use common::Count;
use rsconnect::{clone, Connect, ConnectInterface, ConnectNodeInterface};
mod common;

#[test]
fn update_during_another() {
	let mut conn = Connect::new();
	let mut effects = Vec::new();

	let c_calls: Count = Default::default();
	let c_calls_closure = c_calls.clone();
	let d_calls: Count = Default::default();
	let d_calls_closure = d_calls.clone();

	let a = conn.observed(1);
	let b = conn.observed(2);

	let c = conn.computed(clone!(move |conn| {
		c_calls_closure.increase();
		*conn.get(&a) + *conn.get(&b)
	}));

	effects.push(conn.effected(clone!(move |conn| { *conn.get(&a) }), {
		let mut conn = Connect::new();
		move |a| {
			d_calls_closure.increase();
			conn.set(&b, a + 1);
		}
	}));

	assert_eq!(*conn.get(&a), 1);
	assert_eq!(*conn.get(&c), 3);

	assert_eq!(c_calls.get(), 1); // "c" gets called once initially, and when
							  // effect gets initialized, it doesn't update
							  // because it sets "b" to the same value (2)
	assert_eq!(d_calls.get(), 1);

	conn.set(&a, 2);

	assert_eq!(*conn.get(&c), 5);
	assert_eq!(c_calls.get(), 3); // "c" gets called once due to "a" update,
							  // and another time due to effect changing "b"
	assert_eq!(d_calls.get(), 2);

	assert_eq!(effects.len(), 1);
}
