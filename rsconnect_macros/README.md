# rsconnect_macros
Rust crate for fine-grained reactivity (macros)

[Current documentation is located here.](https://gitlab.com/guitarino/rsconnect/-/blob/main/rsconnect/README.md)
