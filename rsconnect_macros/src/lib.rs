use proc_macro::TokenStream;
use syn::{parse_macro_input, ExprClosure};

mod clone;

#[proc_macro]
pub fn clone(input: TokenStream) -> TokenStream {
	let closure = parse_macro_input!(input as ExprClosure);

	TokenStream::from(clone::clone(closure))
}
