use quote::__private::{Span, TokenStream};
use quote::quote;
use syn::{fold::Fold, Expr, ExprClosure, ExprMethodCall, ExprPath, Ident, Pat};

struct CloneFold {
	connect_name: Ident,
	dependencies: Vec<(Ident, Expr)>,
}

impl Fold for CloneFold {
	fn fold_expr_method_call(&mut self, mut node: ExprMethodCall) -> ExprMethodCall {
		if let ("get", Expr::Path(connect_name), Some(dependency)) = (
			node.method.to_string().as_str(),
			node.receiver.as_ref(),
			node.args.first_mut(),
		) {
			let dependency = if let Expr::Reference(dependency) = dependency {
				dependency.expr.as_mut()
			} else {
				dependency
			};
			if let Some(connect_name) = connect_name.path.segments.first() {
				if connect_name.ident == self.connect_name {
					let existing_dependency = self
						.dependencies
						.iter()
						.find(|existing_dependency| &existing_dependency.1 == dependency);

					if let Some((unique_identifier, _)) = existing_dependency {
						*dependency = Expr::Path(ExprPath {
							attrs: vec![],
							qself: None,
							path: unique_identifier.clone().into(),
						});
					} else {
						let unique_identifier = Ident::new(
							&format!("connect_dependency_{}", self.dependencies.len()),
							Span::call_site(),
						);

						let dependency = std::mem::replace(
							dependency,
							Expr::Path(ExprPath {
								attrs: vec![],
								qself: None,
								path: unique_identifier.clone().into(),
							}),
						);

						self.dependencies.push((unique_identifier, dependency));
					}
				}
			}
		}
		node
	}
}

pub(crate) fn clone(mut closure: ExprClosure) -> TokenStream {
	if let Some(input) = closure.inputs.first() {
		let input = if let Pat::Type(pat_type) = input {
			&pat_type.pat
		} else {
			input
		};
		if let Pat::Ident(input) = input {
			let connect_name = input.ident.clone();

			let mut clone_fold = CloneFold {
				connect_name,
				dependencies: Vec::new(),
			};

			*closure.body = clone_fold.fold_expr(*closure.body);

			let dependencies = clone_fold.dependencies.iter().map(|(left, right)| {
				quote! {
					let #left = #right.clone();
				}
			});

			quote! {
				{
					#( #dependencies )*
					#closure
				}
			}
		} else {
			panic!("clone! closure requires its parameter to be defined as simple identifier, optionally with a type &mut Connect");
		}
	} else {
		panic!("clone! closure requires single argument, of type &mut Connect");
	}
}

#[cfg(test)]
mod clone_macro_tests {
	use super::*;
	use syn::{parse2, parse_quote, Expr, ExprClosure};

	#[test]
	fn basic_identifier_ref_example() {
		let closure: ExprClosure = parse_quote! {
			move |c| *c.get(&revenue) - *c.get(&total_cost)
		};

		let result = parse2::<Expr>(clone(closure)).unwrap();

		let expected_expr: Expr = parse_quote! {
			{
				let connect_dependency_0 = revenue.clone();
				let connect_dependency_1 = total_cost.clone();
				move |c| *c.get(&connect_dependency_0) - *c.get(&connect_dependency_1)
			}
		};

		assert_eq!(result, expected_expr);
	}

	#[test]
	fn self_expr_non_ref_example() {
		let closure: ExprClosure = parse_quote! {
			move |c| *c.get(self.revenue()) - *c.get(self.total_cost())
		};

		let result = parse2::<Expr>(clone(closure)).unwrap();

		let expected_expr: Expr = parse_quote! {
			{
				let connect_dependency_0 = self.revenue().clone();
				let connect_dependency_1 = self.total_cost().clone();
				move |c| *c.get(connect_dependency_0) - *c.get(connect_dependency_1)
			}
		};

		assert_eq!(result, expected_expr);
	}

	#[test]
	fn removing_duplicates_example() {
		let closure: ExprClosure = parse_quote! {
			move |c| if c.get(&idea) {
				use_idea(c.get(&idea))
			} else {
				open(c.get(&mind))
			}
		};

		let result = parse2::<Expr>(clone(closure)).unwrap();

		let expected_expr: Expr = parse_quote! {
			{
				let connect_dependency_0 = idea.clone();
				let connect_dependency_1 = mind.clone();
				move |c| if c.get(&connect_dependency_0) {
					use_idea(c.get(&connect_dependency_0))
				} else {
					open(c.get(&connect_dependency_1))
				}
			}
		};

		assert_eq!(result, expected_expr);
	}

	#[test]
	fn incorrect_connect_name() {
		let closure: ExprClosure = parse_quote! {
			move |connect| c.get(&dep)
		};

		let result = parse2::<Expr>(clone(closure)).unwrap();

		let expected_expr: Expr = parse_quote! {
			{
				move |connect| c.get(&dep)
			}
		};

		assert_eq!(result, expected_expr);
	}

	#[test]
	fn works_with_type_definition() {
		let closure: ExprClosure = parse_quote! {
			move |connect: &mut Connect| connect.get(&dep)
		};

		let result = parse2::<Expr>(clone(closure)).unwrap();

		let expected_expr: Expr = parse_quote! {
			{
				let connect_dependency_0 = dep.clone();
				move |connect: &mut Connect| connect.get(&connect_dependency_0)
			}
		};

		assert_eq!(result, expected_expr);
	}

	#[test]
	fn works_if_more_args_provided() {
		let closure: ExprClosure = parse_quote! {
			move |connect: &mut Connect, previous: f32| connect.get(&dep)
		};

		let result = parse2::<Expr>(clone(closure)).unwrap();

		let expected_expr: Expr = parse_quote! {
			{
				let connect_dependency_0 = dep.clone();
				move |connect: &mut Connect, previous: f32| connect.get(&connect_dependency_0)
			}
		};

		assert_eq!(result, expected_expr);
	}

	#[test]
	#[should_panic(
		expected = "clone! closure requires single argument, of type &mut Connect"
	)]
	fn panics_if_no_arg_provided() {
		let closure: ExprClosure = parse_quote! {
			move || connect.get(&dep)
		};

		parse2::<Expr>(clone(closure)).unwrap();
	}

	#[test]
	#[should_panic(
		expected = "clone! closure requires its parameter to be defined as simple identifier, optionally with a type &mut Connect"
	)]
	fn panics_if_unsupported_arg_provided() {
		let closure: ExprClosure = parse_quote! {
			move |(thing1, thing2)| connect.get(&dep)
		};

		parse2::<Expr>(clone(closure)).unwrap();
	}
}
